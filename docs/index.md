#Mon site FabLab
## À propos de moi

![](images/smallavatar.jpg)

Bonjour ! Je m'appelle Cédric Luppens, j'ai 23 ans et je suis en étudiant en MA2 à 
l'Ecole polytechnique de Bruxelles. Je suis dans l'option biomédicale. 
J'adore voyager et aller marcher dans la nature. Je le fais dès que j'en ai l'occasion, j'aime aussi 
faire du sport. Je suis entraineur de tennis de table pour les jeunes. 



## Previous work
Avant d'arriver à l'université, je n'avais pas vraiment fait de projet. C'est donc en BA1 que j'ai découvert cela avec le 
projet BACAR. Le but était de construire une voiture miniature capable de rouler toute seule.  
Mais si j'ai choisi ce cours aujourd'hui, c'est parce que lors de ma BA2 j'ai dû, pour
la première fois, designer quelque chose en 3D et cela m'a vraiment plu. Depuis, je me suis
perfectionné dans ce domaine et j'espère en apprendre encore plus grâce à ce cours. 
