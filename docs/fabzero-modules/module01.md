# 1. Documentation
Le premier module de ce cours, inspiré par le cours de Neil Gershenfeld "How to Make Almost Anything"[^1], consiste à prendre en main tous les outils qui sont nécessaires
afin d'avoir une bonne documentation.  
  
[^1]:[Neil Gershenfeld - How to Make Almost Anything](https://www.youtube.com/watch?v=EYqt2d0fOr8)  

Les outils suivans seront utilisés et présentés dans ce module :  

- CLI
- Git
- Markdown
- Traitement d'images


## 1.1 Utilisation des commandes dans un terminal (CLI)
Actuellement, beaucoup de programmes utilisent des interfaces graphiques mais un des objectifs 
de ce cours est de mettre en pratique la philosophie UNIX. C'est à dire l'utilisation de
petits programmes qui ne font qu'une chose mais qui le font bien. Ce sont aussi des programmes
conçus pour fonctionner ensemble [^2].

[^2]:[UNIX] (https://en.wikipedia.org/wiki/Unix_philosophy#:~:text=The%20Unix%20philosophy%20emphasizes%20building,as%20opposed%20to%20monolithic%20design.)  

Afin de pouvoir utiliser UNIX, il a fallu activer une fonctionnalié Windows:  Windows Subsystem for Linux
qui permet d'utiliser des commandes Linux à l'intérieur d'un terminal. J'utilise l'application
Ubuntu que j'ai installée depuis le Windows Store.    
Voici un tableau avec quelques commandes de base que j'ai utilisées pendant la session.


| *Command* |Description | *Option* | Description
| :--- | :--- | :--- | :---
|`ls` | *List files and folders*|`ls -a` ou `ls --all` | *Display hidden files*
| | | `ls -l` | *Long list format*
| | | `ls -h` | *Human readable*
| `pwd` | *Print working directory* | |
| `cd` | *Change directory* | `cd -` | *Go to the last directory*
| `mkdir` | *Create a directory* | `mkdir -p` parent/child | Parent directories
|`touch file.txt` | *Create an empty file* | |
| `rmdir` | *Remove empty directories* | |
| `cp file.txt copy.txt` | *Copy files and folders* | |
| `mv file.txt file2.txt` | *Move files or rename files* | `mv -r` | *Recursive*
|`find` | *Recursively search for files and folders inside a specified starting location*
| `echo` | *Display its parameters in the stdout (screen)* | `echo text > file.txt` | *Write test to file (overwrite!)*
| | | `echo text >> file.txt` | *Append text to file (new line)*

## 1.2 Familiarisation avec Git
Personnellement, j'avais déjà utilsé GitLab pour d'autres projets. Je savais donc comment
cloner un projet sur mon oridnateur local. Néanmoins, je ne l'avais jamais fait via une
clé SSH. J'ai donc suivi toute la procédure présentée [ici.](https://docs.gitlab.com/ee/user/ssh.html)  
On peut voir sur cette image les différentes commandes que j'ai tapées afin de générer ma
paire de clés.

![](images/ssh_steps.jpg)

Une fois que les clés sont générées, il est maintenant possible de cloner le projet sur mon ordinateur local en
utilisant la commande git clone.
Maintenant que le projet est cloné, je peux travailler sur cette version locale et ensuite venir mettre le git à jour.
Pour ce faire il faut utiliser 3 commandes. C'est ce que l'on peut voir sur l'image qui suit.
![](images/git_steps.jpg)

## 1.3 Markdown
Markdown est un langage plus facile à utiliser que l'html et qui permet de faire notre page Web.
Pour éditer du markdown j'utilise un éditeur de texte que j'avais déjà installé sur mon ordinateur
pour un autre projet: IntelliJ IDEA. C'est facile à utiliser et il y a une preview du markdown que 
j'écris.
Voici quelques commandes de base en markdown :  

| Commande | Résultat
| :--- | :---
| `# Titre ` | ![](images/titre.JPG)  
| `## Sous titre ` | ![](images/sous_titre.JPG)  
| `### Sous sous titre ` | ![](images/sous_sous_titre.JPG)  
| `mot en **gras**` | mot en **gras**  
| `mot en *italique*` | mot en *italique*  
| `1. premier élement` \n `2. deuxième élement` | ![](images/liste_numerotee.JPG)  
| `- premier élement` \n `- deuxième élement` | ![](images/liste.JPG)  
| ![](images/code_code.JPG) | `code`  
| `Image : ![titre de l'image](image.jpg)` | ![titre de l'image](image)  
| `Lien : [nom du lien](adresse)` | [nom du lien](adresse)  
|`Ceci est le passage[^1] à annoter` | ![](images/note.JPG)  
|`[^1]: ceci est la note de bas de page` | ![](images/bas_page.JPG)  
|`- [x] tâche réalisée` \n `- [ ] tâche à faire` | ![](images/tache.JPG)  
|`> ceci est une citation` | ![](images/citation.JPG)  
|`Une définition` \n ` : le contenu de la définition` | ![](images/definition.JPG)  
|`| première colonne | deuxième colonne | troisième colonne` \n `| :---- | :----: | ----:` \n `| gauche | centré | droite` | ![](images/tableau.JPG)  
|`~~~ première ligne` \n `deuxième ligne ~~~` | ![](images/code_long.JPG)  



## 1.4 Traitement d'images
Afin d'ajouter des images sur la page web et pour que celles-ci ne soient pas trop volumineuses,
le programme GraphicsMagick est utilisé dans ce cours. L'installation est assez facile. Le logiciel
s'utilise depuis le terminal de commandes et permet, très simplement, de changer la taille d'une image.
Il y a quelques commandes de base qui sont utiles à connaitre :  

|Commande|Description |Options
|:--- | :--- |:---
| `composite` | *composite images together* | `-resize` geometry, `-background` color
|`convert` | *convert an image or sequence of images* | `-compress` type, `-crop` geometry, `-quality` value, `-resize` geometry
|`montage` | *create a composite image (in a grid) from separate images* | `-adjoin`, `-compose` operator

La commande que j'utilise le plus est ` gm convert -resize 500x400 example.png small_example.jpg`.
Elle permet de réduire la taille de l'image et ainsi le volume que celle-ci occupe.


