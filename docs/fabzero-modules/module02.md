# 2. Conception Assistée par Ordinateur (CAO)
Les outils de conception numérique sont des outils puissants, ils permettent de dessiner des objets en 3D et de les modifier très facilement et autant qu'on le souhaite.
Il est également possible de partir de modèles existants, principalement si on utilise des logiciels open source, afin de les adapter à la pièce que l'on veut designer.
## 2.1 Les mécanismes flexibles
Le but de ce module est de concevoir un [Flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks).  
Ces flexlinks se basent sur un mécanisme appelé [Compliant mechanism](https://en.wikipedia.org/wiki/Compliant_mechanism).  
Ce mécanisme est utilisé pour fabriquer des "machines" qui ne possèdent pas
de pièces mobiles mais dans lesquelles il y a quand même du mouvement et ce grâce au design de la pièce
mais aussi aux propriétés des matériaux utilisés. 

## 2.2 Logiciels 3D
### 2.2.1 Encodage des images
Tout d'abord, il est possible de travailler sur une image en 2D. Pour ce faire, l'image peut être
encodée de 2 façons différentes. A l'aide de pixels ou de manière vectorielle. 
Pour la première méthode, l'image est une matrice où chaque nombre représente l'intensité de gris ou de couleur.
Au contraire, une image vectorielle est composée de formes géométriques qui sont contraintes ensemble à l'aide d'un code
afin de former l'image.
Les images vectorielles peuvent donc être zoomées à l'infini ce qui n'est pas le cas des 
images matricielles.  
### 2.2.2 Conception d'un flexlink  
Pour la conception du flexlink en tant que tel, 2 logiciels open source sont proposés :  
-OpenSCAD  
-FreeCAD  
Il est, en effet, préférable d'utiliser des logiciels open source car ceux-ci permettent plus facilement le 
partage de pièces entre différentes personnes. Or c'est aussi le but de ce module, d'utiliser
la pièce de quelqu'un d'autre afin d'assembler plusieurs flexlinks.  
Personnellement, j'ai choisi FreeCAD pour désigner ma pièce car j'ai déja fait beaucoup de design 3D
sur le logiciel SolidWorks et FreeCAD est celui qui lui ressemble le plus. OpenSCAD est totalement différent,
il faut écrire du code pour construire sa pièce.  

| OpenSCAD |FreeCAD|
| :--- | :--- 
|![OpenSCAD](images/small_sphere.jpg) |![FreeCAD](images/small_free.jpg)

|SolidWorks|
| :---
|![SolidWorks](images/small_sd.jpg)

On peut clairement voir la ressemblance entre FreeCAD et SolidWorks.  
De plus, FreeCAD est plus complet et permet également d'ouvrir des pièces venant d'OpenSCAD.  
Avec 2 autres élèves du cours (Jonathan Kahan et Dimitri Debauque), on a décidé de fabriquer
une voiture et la partie flexible est un ressort qui représente les suspensions.
Ma mission était donc de désigner ce ressort.  
Pour cela, j'ai commencé par dessiner une hélice à l'aide de la fonction `créer des primitives`,
comme l'on peut le voir sur l'image suivante.
![](images/small_des.jpg)

Ensuite, j'ai créé une esquisse en forme de cercle au début de cette hélice.
![](images/small_sk.jpg)

Pour terminer, j'ai fait un balayage de ce cercle le long de l'hélice.
![](images/small_bal.jpg)

Pour finalement avoir le ressort :  
![](images/small_ressort.jpg)

Il est important que le ressort soit flexible et imprimable. Afin d'être sur que ce soit le cas,
il va probablement falloir changer plusieurs fois les dimensions du ressort. Il
faut donc que celui-ci soit paramétrique. C'est possible de faire ça à l'aide du module Spreadsheet qui ressemble à une tableau
Excel et où il est facile de définir des paramètres.

La pièce FreeCAD est accessible en suivent ce [lien.](images/Spring.FCStd1)

## 2.3 Licences
Il est important de mettre une licence sur ses pièces afin que celles-ci puissent être 
réutilisées dans le futur. Par défaut, un travail n'a pas le droit d'être réutilisé.
Il existe plusieurs types de licence, elles sont toutes expliquées en détail sur ce [site.](https://creativecommons.org/about/cclicenses/)
Dans le cadre de ce cours, j'ai choisi d'utiliser la license `CC BY_SA`. 
Cette licence permet aux réutilisateurs de distribuer et de modifier le travail sur n'importe quel support ou format, 
à condition que le créateur soit mentionné. 
Si vous modifiez la pièce, vous devez accorder la même licence à votre modification.







