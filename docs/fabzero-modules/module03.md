# 3. Impression 3D
Cette semaine, le cours était une formation à l'impression 3D.
Nous avons tout d'abord suivi un tutoriel sur le logiciel Prusa slicer. Ensuite, nous
avons pu utiliser les imprimantes présentes au FabLab afin d'imprimer 
le Flexlink conçu dans le module 2.

## 3.1 Prusa slicer
Le logiciel peut être téléchargé directement sur [le site de Prusa.](https://help.prusa3d.com/article/install-prusaslicer_1903)
Le tutorial que nous avons suivi est disponible sur [la page du cours.](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md)

Personnellement, j'avais déjà imprimé plusieurs pièces pour 3D pour des projets à l'université mais j'ai quand
même appris pas mal de chose, surtout sur les limites de l'imprimante.
En effet, celle-ci ne sait pas imprimer toutes les pièces. Il y a certains angles, épaisseurs à respecter.
Pour tester les limites d'une imprimante il est possible d'imprimer une pièce comme [celle-ci](https://www.thingiverse.com/thing:4214436) :  

![](images/torture_test.jpg)

## 3.2 Impression du Flexlink
Après avoir suivi le tutoriel, je me suis vite rendu compte que mon ressort ne
s'imprimerait pas correctement. La surface d'adhérence avec le plateau étant trop petite.

![](images/small_spring.jpg)

J'ai donc décidé de rajouter une petite base afin que l'impression
soit possible. 

![](images/small_supp.jpg)

Dans ce cas-ci, la base n'empêche pas de faire ce qui était prévu avec le ressort
(suspension de voiture) mais si c'était le cas, il est toujours possible d'utiliser un support lors de l'impression 3D.
C'est une des options possibles sur le slicer. Une fois l'impression
finie, il suffit d'enlever le support de la pièce. (Cela peut parfois être compliqué de
le faire proprement.)

![Avec support](images/small_sp.jpg)

C'est donc la pièce avec la base qui a été imprimée, néanmoins celle-ci était ratée.
Il y avait un problème avec l'angle de surplomb, comme on peut le voir sur 
l'image juste au-dessus avec toute une partie de la surface qui est en bleu foncée.

![](images/sm_ressort_nul.jpg)

Comme la pièce a été designée de manière paramétrique, il a été
très facile de la modifier. Il a fallu changer l'épaisseur du ressort mais aussi
l'écart entre les spirales.

![](images/sm_ressort_bon.jpg)

Cette fois-ci, on peut voir que l'impression n'est pas parfaite
mais déjà beaucoup mieux que le premier essai. Il faudrait encore augmenter l'écart entre
les spirales. Néanmoins, comme on peut le voir sur la vidéo suivante, le ressort est flexible comme voulu.
Il a donc été décidé de garder cette pièce.

<video controls muted>
<source src="../images/video.mp4" type="video/mp4">
</video>

![](images/video.mp4)

## 3.3 Assemblage des différentes parties
Avec Dimtri et Jonathan, nous avons assemblé nos différentes pièces afin d'avoir une voiture. 
Après avoir du modifier les dimensions à plusieurs reprises, voici quelques photos et vidéos.
![](images/small_voiture1.jpg)
![](images/small_voiture2.jpg)

<video controls muted>
<source src="../images/voiture.mp4" type="video/mp4">
</video>

![](images/voiture.mp4)

<video controls muted>
<source src="../images/voiture1.mp4" type="video/mp4">
</video>

![](images/voiture1.mp4)

<video controls muted>
<source src="../images/voiture2.mp4" type="video/mp4">
</video>

![](images/voiture2.mp4)



