# 4. 3D Repair
Cette semaine, la formation que j'ai suivie est la formation 3D Repair. 

## 4.1 Déroulement de la formation

Tout d'abord, dès que je suis arrivé, j'ai reçu une boite contenant un simple circuit éléctrique
qui ne fonctionnait pas. Le but était de trouver la panne et de la réparer.
Cette première étape nous a permis de directement tester et voir par nous les
mêmes les différents problèmes que l'on peut rencontrer lors de la réparation d'objets mais aussi 
à quoi l'on doit faire attention. 
Personnellement, ma boite contenait un fil électrique qui était cassé.
L'une des premières choses où il faut faire attention, c'est ranger les vis dans un même endroit.
Cela peut paraitre stupide, mais il n'est pas rare de perdre des vis après avoir démonter un objet ou
de ne plus savoir quelle vis va où. Pour éviter ela, il vaut mieux toujours prendre une photo de l'objet avant de 
le démonter et d'avoir un petit pot pour ranger les vis.
Un deuxième point, lorsqu'on travaille avec des circuits électriques, ce qui est souvent le cas de nos jours,
est d'être sur que l'alimentation est coupée et que le circuit n'est pas branché avant de commencer à travailler dessus.  
Après cela, nous avons suivi une formation en tant que telle, nous avons parcouru des slides concernant la réparation d'une pièce que l'on veut 
imprimer en 3D. Dans ces slides, il y a tout un procédé qui est expliqué afin d'optimiser les chances que 
la réparation marchera. Le même procédé est expliqué dans ce [guide](https://textbooks.open.tudelft.nl/textbooks/catalog/book/49).

## 4.2 Réparation de notre objet

Par groupe de 2, j'ai formé un groupe avec [Christophe](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/christophe.ory/), 
nous devions réparé un objet. Nous avons reçu une valise dont le bouton qui permet de sortir et rentrer
la poignée était cassé. La pièce en plastique à l'intérieur était cassée en 2.
![](images/small_v_casse.jpg)  
Tout d'abord, après avoir démonté la valise, nous avons un peu analysé la situation.
C'est à dire, comment le mécanisme fonctionne, quel est le rôle de cette pièce, doit-elle être redesignée
exactement de la même façon ou peut-elle être simplifiée? Quelles sont les forces que cette pièce va subir?  
Après avoir fait cela, l'on s'est vite rendu compte que le design arrondi n'était pas nécessaire et qu'il y avait moyen de designer quelque chose de plus simple.
Avant de commencer à designer la pièce en 3D, nous avons pris toutes les mesures et fait un plan 2D
de la pièce que l'on voulait désigner. Toutes les mesures ont été faites à l'aide d'un pied à coulisse.
Etant déjà très familier avec SolidWorks et dans le but d'avoir un résultat final qui fonctionne, nous
avons décidé de faire cette pièce en 3D sur SolidWorks.
Voici notre premier idée de design : 
![](images/small_valise.jpg) ![](images/small_valise2.jpg)

Avant d'imprimer la pièce, il faut réfléchir au sens de l'impression afin d'être sur que la pièce soit résistante aux forces dans la bonne direction mais aussi qu'il est possible de l'imprimer.
Nous avons donc imprimé cette pièce mais malheureusement, il y avait un petit souci dans nos 
mesures et la pièce ne s'emboitait pas. De plus, le bouton carré ne rentrait pas dans l'ouverture qui est plus arrondie,
nous avons donc arrondi le bouton et réglé les problèmes de mesure.  
Nous avions décidé de changer le sens de l'impression mais ce n'était malheureusement pas une bonne idée.
Nous avons du ajouter des supports et ceux-ci n'ont pas été faciles à retirer, de plus l'impression du dessus n'était pas très bien faite.  
Il a donc fallu imprimer un troisième modèle, celui-ci s'emboitait bien mais les bouts carrés qu'on peut voir sur la photo
étaient trop longs, du coup la poignée de la valise s'abaissait tout le temps.
On a réduit ça , ça fonctionne mais pas de manière parfaite comme on peut le voir sur cette vidéo.
Il faut forcer pour que cela fonctionne.

<video controls muted>
<source src="../images/video_v_bug.mp4" type="video/mp4">
</video>

![](images/video_v_bug.mp4)

On a donc décidé d'imprimer un tout dernier modèle pour que tout soit parfait. Il fonctionne vraiment
super bien.

<video controls muted>
<source src="../images/video_v_ok.mp4" type="video/mp4">
</video>

![](images/video_v_ok.mp4)

Au final, nous avons imprimé 5 [modèles](images/valise.SLDPRT), mais la valise refonctionne parfaitement.  
![](images/small_model_v_all.jpg) ![](images/small_model_v_all2.jpg)
