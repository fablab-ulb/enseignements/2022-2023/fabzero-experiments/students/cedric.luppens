# 5. Dynamique de groupe et projet final
## 5.1 Analyse de projet
La première partie de ce module consiste à choisir un projet que l'on 
trouve important et de faire un arbre des problèmes et des objectifs sur la thématique de ce projet.
Personnellement, j'ai choisi le [projet](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-experiments/students/andrewkaram/final-project/) d'un étudiant de l'année passée.
Celui-ci a pour but de créer un appareil permettant de surveiller sa consommation d'eau lorsqu'on
prend une douche.  
C'est directement lié à l'objectif de développement durable
 numéro 6 de l'ONU.
 ### 5.1.1 Arbre des problèmes
 
 ![](images/tree_pb.jpg)
 
 ### 5.1.2 Arbre des objectifs
 
 ![](images/arbre_objectif.jpg)
 
 ## 5.2 Brainstorming et formation des groupes
 Tout d'abord, pour la séance d'aujourd'hui, il a fallu amener un
 objet qui représente une problématique qui nous tient à coeur.
Ensuite, tout le monde montrait son objet et le but était de se rapprocher
des personnes qui avaient amené un objet qu'on pensait avoir une thématique
commune avec le notre.  
L'objet que j'ai choisi est une craie (pour écrire en classe) et cela
représente les inégalités présentes dans l'enseignement que ce soit à l'étranger mais aussi en Belgique. C'est lié à l'objectif
de développement numéro 4 de l'ONU. 
### 5.2.1 Formation du groupe 
Quand il a fallu aller vers d'autres personnes, il n'y avait pas énormément d'objets qui faisaient penser
à ma thématique. C'est surtout un objet qui avait attiré mon attention. En effet, quelqu'un
avait amené un microscope pour enfant. Quand j'ai eté lui parler, il parlait déjà avec quelqu'un qui avait amené une loupe.
Nos thématiques étaient un peu différentes mais liées. Le garçon qui avait amené la loupe ([Kamel](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/kamel.sanou/))
a choisi une loupe pour représenter les inégalités de manière générale, pas spécialement
dans l'enseignement. L'autre garçon ([Thibault](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/leonard.thibault/))
a amené un microscope pour représenter le manque d'intérêt grandissant des jeunes pour la science.
De nouveau, pas exactement la même chose mais avec des points communs. On a donc décidé de se mettre
à 3 sachant que les autres thématiques étaient totalement différentes. Quelques minutes après, on a été rejoint par un autre garçon ([Stanislas](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/stanislas.mondesir/))
qui a amené un cable éthernet. Celui-ci représentait les écoles primaires qui n'ont pas accès à internet et 
par conséquent, bénéficie de moins bonnes conditions d'enseignement.
Cette fois-ci, c'était totalement lié à ma thématique. On a donc formé un groupe à 4.

### 5.2.2 Choix de la thématique
Après un temps de discussion entre nous afin de se découvrir, il était temps de choisir une thématique.
Afin de choisir une thématique qui convient à tout le monde,
ce n'est pas la majorité qui l'emporte. En effet, il faut que chacun écrive sur un post-it qu'elle est la thématique que lui voudrait et
ensuite tous les membres du groupe mettent une note à chaque thématique (entre 1 et 4 par exemple).
Dans notre groupe, cela été facile car lors de la phase de brainstorming et discussion, nous sommes tombés d'accord sur une thématique globale qui est l'enseignement et l'éducation.

### 5.2.3 Choix d'une problématique
Maintenant que nous avons choisi notre thématique, il a fallu choisir
une problématique. Pour ce faire, à tour de rôle chaque membre du groupe devait
énoncer une problématique en commençant sa phrase par "À ta place, je ...".
Le but était que l'exrcice soit dynamique et que même si un de membres du groupe
n'a plus d'idées, il doit juste dire "je passe" afin que l'exercice continue avec ceux qui ont encore des idées.
Ca donne aussi l'occasion de réfléchir un peu plus, un moment j'ai dit je passe, puis au tour d'après j'avais de nouveau des idées.

La dernière étape du processus consistait à désigner un porte parole qui 
va aller dans les autres groupes et expliquer la thématique choisie par notre groupe afin de récolter
de nouvelles idées de problématique. J'ai eu ce rôle de porte parole, j'ai été dans 2 autres groupes.
Ils ont cité des problématiques communes à celles qu'on avait trouvées mais aussi des nouvelles.
Néanmoins, cela restait assez vague. On avait pas encore d'idée précise d'une problématique. Mais on garde en tête
notre idée d'inégalités dans l'enseignement.  

##5.3 Dynamique de groupe
Malheureusement, je n'ai pas pu assister à ce cours :-( .

 
 